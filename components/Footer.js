
function Footer() {
  return (
    <div className="bg-white-300 pt-44 pb-24">
      <div className="max-w-screen-xl w-full mx-auto px-6 sm:px-8 lg:px-16 grid grid-rows-6 sm:grid-rows-1 grid-flow-row sm:grid-flow-col grid-cols-3 sm:grid-cols-12 gap-3">
        <div className="row-span-2 sm:col-span-4 col-start-1 col-end-4 sm:col-end-5 flex flex-col items-start ">
          <img
            alt="logo"
            height={40}
            width={155}
            className="mr-1 object-contain"
            src="https://tekup.vn/wp-content/uploads/2022/08/logo-tekup-02.png"
          />
          <p className="mb-4 mt-8 text-palette-grey">
            Strategic Products – Transformative Business.
          </p>
        </div>
        <div className="row-span-2 sm:col-span-3 sm:col-start-9 sm:col-end-11 flex flex-col">
          <p className="text-black-600 mb-4 font-medium text-lg">Company</p>
          <ul className="text-black-500 ">
            <li className="my-2 hover:text-orange-500 cursor-pointer transition-all">
              About Tekup{" "}
            </li>
            <li className="my-2 hover:text-orange-500 cursor-pointer transition-all">
              Contact{" "}
            </li>
            <li className="my-2 hover:text-orange-500 cursor-pointer transition-all">
              Privacy Policy{" "}
            </li>
            <li className="my-2 hover:text-orange-500 cursor-pointer transition-all">
              Terms of use{" "}
            </li>
          </ul>
        </div>
        <div className="row-span-2 sm:col-span-3 sm:col-start-11 sm:col-end-13 flex flex-col">
          <p className="text-black-600 mb-4 font-medium text-lg">Services</p>
          <ul className="text-black-500">
            <li className="my-2 hover:text-orange-500 cursor-pointer transition-all">
              Comprehensive Solution Providing
            </li>
            <li className="my-2 hover:text-orange-500 cursor-pointer transition-all">
              Web / App Developing
            </li>
            <li className="my-2 hover:text-orange-500 cursor-pointer transition-all">
              IT Workforce Supply (OSDC)
            </li>
            <li className="my-2 hover:text-orange-500 cursor-pointer transition-all">
              UX/UI Design
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
}

export default Footer
