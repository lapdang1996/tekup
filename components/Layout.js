import { CartProvider } from '@/context/Store'
import Nav from '@/components/Nav'
import Footer from '@/components/Footer'

function Layout({ children }) {
  
  return (
    <CartProvider>
      <div className="flex flex-col justify-between min-h-screen">
        <div>
          <div className="relative h-112 w-full bg-black flex items-center justify-center">
            <div className='text-6xl font-semibold text-white'>Projects</div>
          </div>
        </div>
        <Nav />

        <main>{children}</main>

        <Footer />
      </div>
    </CartProvider>
  );
}

export default Layout
