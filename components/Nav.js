import { useState, useEffect } from 'react'
import Link from 'next/link'
import { useCartContext } from '@/context/Store'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faShoppingCart } from '@fortawesome/free-solid-svg-icons'

function Nav() {
  const cart = useCartContext()[0]
  const [cartItems, setCartItems] = useState(0)
  const [show, setShow] = useState(false)

  useEffect(() => {
    let numItems = 0
    cart.forEach(item => {
      numItems += item.variantQuantity
    })
    setCartItems(numItems)
  }, [cart])

  useEffect(() => {
    const element = document.documentElement;
    
    const handleWindowScroll = () => {
      if (element.scrollTop > 200) setShow(true)
      else setShow(false)
    }
    window.addEventListener('scroll', handleWindowScroll, false)
    return () => window.removeEventListener('scroll', handleWindowScroll, false)

  }, [])

  return (
    <header
      className={`"border-b border-palette-lighter top-4 z-20 bg-white w-calc-80 rounded-lg left-10 ${
        show ? "hidden" : "fixed"
      }`}
    >
      <div className="flex items-center justify-between mx-auto max-w-6xl px-3 pb-2 pt-3 md:pt-2">
        <Link href="/" passHref>
          <a className=" cursor-pointer">
            <h1 className="flex no-underline">
              <img
                alt="logo"
                height={40}
                width={155}
                className="mr-1 object-contain"
                src="https://tekup.vn/wp-content/uploads/2022/08/logo-tekup-02.png"
              />
            </h1>
          </a>
        </Link>
        <div className="flex items-center justify-between">
          {["Home", "About Us", "Services", "Projects", "Contact", "Blog"].map(
            (item) => (
              <Link href="/" passHref>
                <a className=" cursor-pointer pl-6 pr-6">
                  <h1 className="flex">
                    <span className="text-[14px] text-palette-grey font-primary font-bold tracking-tight pt-1 relative after:bg-black after:absolute after:h-1 after:w-0 after:bottom-0 after:left-0 hover:after:w-full after:transition-all after:duration-300 cursor-pointer">
                      {item}
                    </span>
                  </h1>
                </a>
              </Link>
            )
          )}
        </div>
      </div>
    </header>
  );
}

export default Nav
