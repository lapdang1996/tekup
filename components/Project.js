import { useState } from 'react'
import ProjectCard from '@/components/ProjectCard'
import ProjectShared from "@/components/ProjectShared"

function Project() {
  const [activeTab, setActiveTab] = useState(0);
  const tabs = ["All", "Manpower Supply", "Mobile App / Websites", "UI/UX Design"];
  const projects = [
    {
      id: 1,
      img: "https://tekup.vn/wp-content/uploads/2022/09/image.png",
      title: "E-learning – Internal training platform",
    },
    {
      id: 1,
      img: "https://tekup.vn/wp-content/uploads/2022/08/summer21-thumbnail.png",
      title: "Summer 21 Cosmetic E-commerce Platform",
    },
    {
      id: 1,
      img: "https://tekup.vn/wp-content/uploads/2022/08/kiva-thumbnail.png",
      title:
        "Kiva – Ambition to digital transformation in the brokerage assiduity",
    },
    {
      id: 1,
      img: "https://tekup.vn/wp-content/uploads/2022/09/image.png",
      title: "E-learning – Internal training platform",
    },
  ];

  const handleOnchange = (index) => {
    setActiveTab(index);
  }
  return (
    <div>
      <div className="flex justify-center mt-20">
        {tabs.map((item, index) => (
          <div
            onClick={() => handleOnchange(index)}
            key={`project-${item}`}
            className={
              activeTab == index
                ? "text-[#212529] py-2 px-4 bg-yellow text-[18px] shadow rounded-lg font-bold"
                : "text-[#212529] py-2 px-4 text-[18px] font-bold"
            }
          >
            {item}
          </div>
        ))}
      </div>
      <div className="mt-4">
        <div className="py-12 max-w-6xl mx-auto grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 gap-x-4 gap-y-8">
          {projects.map((item, index) => (
            <ProjectCard key={index} project={item} />
          ))}
        </div>
      </div>
      <ProjectShared />
    </div>
  );
}

export default Project;
