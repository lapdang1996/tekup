
import Link from "next/link";

function ProjectCard({ project }) {

  return (
    <Link href={`/projects/${project.id}`} passHref>
      <a className="h-[380px] w-[380px] rounded shadow-lg mx-auto border border-palette-lighter relative">
        <div className="h-[390px] border-b-2 border-palette-lighter relative">
          <img
            src={project.img}
            alt={project.img}
            layout="fill"
            className="transform duration-500 ease-in-out hover:scale-105"
          />
        </div>
        <div className="p-4 absolute width-calc-32 bottom-4 mx-4 bg-white rounded flex items-center">
          <div className="font-primary text-[#212529] text-[1.125rem] px-4 font-bold">
            {project.title}
          </div>
          <svg
            class="overflow-visible ml-3 text-slate-300 group-hover:text-slate-400 dark:text-slate-500 dark:group-hover:text-slate-400"
            width="3"
            height="6"
            viewBox="0 0 3 6"
            fill="none"
            stroke="currentColor"
            stroke-width="2"
            stroke-linecap="round"
            stroke-linejoin="round"
          >
            <path d="M0 0L3 3L0 6"></path>
          </svg>
        </div>
      </a>
    </Link>
  );
}

export default ProjectCard;
