
import Link from "next/link";

function ProjectShared({ project }) {
  return (
    <div className="bg-shared bg-705 h-705 bg-center bg-no-repeat flex items-center">
      {/* <div className="bg-shared w-705 h-705 absolute top-1/2 left-1/4  bg-705"></div> */}
      <div className="bg-yellow px-[90px] py-[30px] h-270 w-full rounded-xl flex items-center justify-evenly">
        <div></div>
        <img
          style={{ marginTop: "-25px" }}
          src={"https://tekup.vn/wp-content/uploads/2022/01/spaceship.png"}
          alt="spaceship"
          layout="fill"
          className="w-230"
        />
        <div className="w-600">
          <h4 className="text-palette-grey text-3xl font-semibold">
            Are you ready to share your ideas, your projects with us?
          </h4>
          <div className="text-[18px] mt-4 text-palette-grey">
            Let your project summaries come true by calling Tekup or sending
            Tekup emails{" "}
          </div>
        </div>
        <div className="bg-black px-8 py-4 text-white rounded">Contact Us</div>
      </div>
    </div>
  );
}

export default ProjectShared;
