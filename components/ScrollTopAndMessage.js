import { useEffect, useState } from 'react'

const ScrollTopAndMessage = (myRef) => {
  const [show, setShow] = useState(false)

  useEffect(() => {
    const element = document.documentElement;
    
    const handleWindowScroll = () => {
      if (element.scrollTop > 200) setShow(true)
      else setShow(false)
    }
    window.addEventListener('scroll', handleWindowScroll, false)
    return () => window.removeEventListener('scroll', handleWindowScroll, false)

  }, [])

  const handleScrollTop = () => {
    document.documentElement.scrollTo({ top: 0 });
  }

  return (
    <div>
      <div
        className={`fixed bottom-8 left-8 z-[50000] hidden flex-col gap-3 ${
          show ? 'md:flex' : 'md:hidden'
        }`}
      >
        <button
          aria-label="Scroll To Top"
          type="button"
          onClick={handleScrollTop}
          className="rounded-full bg-gray-200 p-2 text-gray-500 transition-all hover:bg-gray-300 dark:bg-gray-700 dark:text-gray-400 dark:hover:bg-gray-600"
        >
          <svg className="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
            <path
              fillRule="evenodd"
              d="M3.293 9.707a1 1 0 010-1.414l6-6a1 1 0 011.414 0l6 6a1 1 0 01-1.414 1.414L11 5.414V17a1 1 0 11-2 0V5.414L4.707 9.707a1 1 0 01-1.414 0z"
              clipRule="evenodd"
            />
          </svg>
        </button>
      </div>
       <div
        className="fixed bottom-8 right-8 z-[50000] flex flex-col gap-3"
      >
        <div style={{cursor: "pointer"}}><div><div style={{display: "flex", alignItems: "center"}}><div style={{width: "60px", height: "60px", backgroundColor: "#0A7CFF", display: "flex", justifyContent: "center", alignItems: "center", borderRadius:"60px"}}><svg width="36" height="36" viewBox="0 0 36 36"><path fill="white" d="M1 17.99C1 8.51488 8.42339 1.5 18 1.5C27.5766 1.5 35 8.51488 35 17.99C35 27.4651 27.5766 34.48 18 34.48C16.2799 34.48 14.6296 34.2528 13.079 33.8264C12.7776 33.7435 12.4571 33.767 12.171 33.8933L8.79679 35.3828C7.91415 35.7724 6.91779 35.1446 6.88821 34.1803L6.79564 31.156C6.78425 30.7836 6.61663 30.4352 6.33893 30.1868C3.03116 27.2287 1 22.9461 1 17.99ZM12.7854 14.8897L7.79161 22.8124C7.31238 23.5727 8.24695 24.4295 8.96291 23.8862L14.327 19.8152C14.6899 19.5398 15.1913 19.5384 15.5557 19.8116L19.5276 22.7905C20.7193 23.6845 22.4204 23.3706 23.2148 22.1103L28.2085 14.1875C28.6877 13.4272 27.7531 12.5704 27.0371 13.1137L21.673 17.1847C21.3102 17.4601 20.8088 17.4616 20.4444 17.1882L16.4726 14.2094C15.2807 13.3155 13.5797 13.6293 12.7854 14.8897Z"></path></svg></div></div></div></div>
      </div>
    </div>
  )
}

export default ScrollTopAndMessage
