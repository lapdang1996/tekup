import PageTitle from '@/components/PageTitle'

function StoreHeading() {
  return (
    <div className="">
      <PageTitle text="Tekup JSC!" />
      <p className="max-w-xl text-center px-2 mx-auto text-base text-gray-600">
        Tekup JSC.    
      </p>
    </div>
  )
}

export default StoreHeading
