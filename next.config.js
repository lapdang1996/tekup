const withPWA = require('next-pwa');

module.exports = withPWA({
  future: { webpack5: true },
  pwa: {
    dest: "public",
    disable: process.env.NODE_ENV === "development",
  },
  env: {
    siteTitle: "Tekup JSC!",
    siteDescription: "Tekup JSC!",
    siteKeywords: "Tekup JSC!",
    siteUrl: "https://tekup.vn/en/projects/",
    siteImagePreviewUrl: "/images/main.jpg",
    twitterHandle: "@deepwhitman",
  },
  images: {
    domains: ["tekup.com"],
  },
});
