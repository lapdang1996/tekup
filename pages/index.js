import Project from "@/components/Project";
import ScrollTopAndMessage from "@/components/ScrollTopAndMessage";

function IndexPage({ products }) {

  return (
    <div className="mx-auto max-w-6xl" id="scrollTargetElementRef">
      <Project />
      <ScrollTopAndMessage />
    </div>
  );
}

export default IndexPage
