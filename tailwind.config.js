module.exports = {
  purge: ["./pages/**/*.{js,ts,jsx,tsx}", "./components/**/*.{js,ts,jsx,tsx}"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      height: (theme) => ({
        112: "28rem",
        120: "30rem",
        705: "705px",
        270: "270px"
      }),
      minHeight: (theme) => ({
        80: "20rem",
      }),
      colors: {
        palette: {
          lighter: "#F5F3FF",
          light: "#DDD6FE",
          primary: "#5B21B6",
          dark: "#4C1D95",
          grey: "#21252a",
        },
      },
      width: {
        "calc-80": "calc(100% - 80px)",
        "calc-32": "calc(100% - 32px)",
        705: "705px",
        230: "230px",
        600: "600px",
      },
      fontFamily: {
        primary: ['"Josefin Sans"'],
      },
      backgroundColor: {
        yellow: "#F8D000",
      },
      backgroundImage: {
        "shared": "url('https://tekupjsc.tekup.vn/wp-content/uploads/2022/01/global-web-01.png')",
      },
      backgroundSize: {
        705: '705px 705px'
      },
      marginTop: {
        25: "-25px"
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [
    require("@tailwindcss/forms")({
      strategy: "class",
    }),
  ],
};
